"""
Do all kinds of ad-hoc stuff here.
"""
from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    pass

from pathlib import Path

from dfylum.common import OrderedEnum
from dfylum._legacy.record_array import RecordArrayBase
from dfylum.structures.record_list import RecordList
from pydantic import BaseModel

X = RecordArrayBase
Y = RecordArrayBase[int]


class ElemSource(OrderedEnum):
    SRC1 = 1
    SRC2 = 2
    SRC3 = 3
    SRC4 = 4


def dyn_enum():
    attrs_dict = {
        "SRC1": 1,
        "SRC2": 2,
        "SRC3": 3,
        "SRC4": 4,
    }

    collation = OrderedEnum(
        "ElemSource", attrs_dict
    )

    return collation


class Element(BaseModel):
    src: ConfigLayer


class PidanConfig(Element):
    conn: str | None = None  # connection string
    mode: str | None = None  # 全量，增量
    schema_path: Path | None = None


class ConfigLayer(BaseModel):
    name: str

CommandLineConfigFile = ConfigLayer(name="CommandLineConfigFile")
CommandLineArgs = ConfigLayer(name="CommandLineArgs")
EnvVar = ConfigLayer(name="EnvVar")
UserConfigFile = ConfigLayer(name="UserConfigFile")
SystemConfigFile = ConfigLayer(name="SystemConfigFile")


class ConfigLayers(BaseModel):
    CommandLineConfigFile: PidanConfig = None
    CommandLineArgs: PidanConfig = None
    EnvVar: PidanConfig = None
    UserConfigFile: PidanConfig = None
    SystemConfigFile: PidanConfig = None
    _rec_list_cls: type[RecordList] | None = None

    class Config:
        underscore_attrs_are_private = True

    @property
    def collation(self) -> type[OrderedEnum]:
        attrs_dict = dict(self.__fields__.items())

        collation: type[OrderedEnum] = OrderedEnum(f"{self.__class__.__name__}Collation", attrs_dict)
        return collation

    @property
    def rec_list_cls(self):
        if self._rec_list_cls is not None:
            return self._rec_list_cls
        class ConfigRecords(RecordList, elem_cls=PidanConfig, id_field='src'):
            ...
        self._rec_list_cls = ConfigRecords
        return ConfigRecords

    @property
    def layers(self) -> tuple:
        return tuple(self.__fields__)

    def record_list_view(self):
        return self.rec_list_cls(*tuple(dict(self).values()))


class ConfigRecords(RecordList[PidanConfig], elem_cls=PidanConfig, id_field='src'):
    ...

    def push(self, elem: PidanConfig):
        ...

PidanConfig.update_forward_refs()
sys_conf = PidanConfig(conn="http://abc", src=SystemConfigFile)
user_conf = PidanConfig(conn="http://bcd", mode="全量", src=UserConfigFile)

pidan_conf_layers = ConfigLayers(SystemConfigFile=sys_conf, UserConfigFile=user_conf)

pidan_conf_layers.record_list_view()
breakpoint()



