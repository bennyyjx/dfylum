from __future__ import annotations

from typing import TYPE_CHECKING


if TYPE_CHECKING:
    pass

from pathlib import Path
from typing import Generic, TypeVar

from pydantic import BaseModel
from pydantic.generics import GenericModel


class ConfigLayer(BaseModel):
    name: str


class Element(BaseModel):
    src: ConfigLayer

ElemT = TypeVar("ElemT", bound=Element)

class RecordArray2(GenericModel, Generic[ElemT]):
    _elem_cls: type
    _elem_src_field: str = "src"
    _layers: list = None
    _data: list = None
    _size: int = None
    class Config:
        underscore_attrs_are_private = True

    @classmethod
    def build(cls, layers) -> RecordArray2[ElemT]:
        self = cls()
        self._layers = layers
        self._size = len(layers)
        self._data = [None] * self._size
        return self

    def calc_idx(self, val: ElemT) -> int:
        src = getattr(val, self._elem_src_field)
        try:
            idx = self._layers.index(src)
        except ValueError as e:
            raise e
        else:
            return idx

    def place(self, val: ElemT) -> RecordArray2[ElemT]:
        idx = self.calc_idx(val)
        self._data[idx] = val
        return self

    def merge(self) -> ElemT:
        ...


class PidanConfig(Element):
    conn: str | None = None  # connection string
    mode: str | None = None  # 全量，增量
    schema_path: Path | None = None


CommandLineConfigFile = ConfigLayer(name="CommandLineConfigFile")
CommandLineArgs = ConfigLayer(name="CommandLineArgs")
EnvVar = ConfigLayer(name="EnvVar")
UserConfigFile = ConfigLayer(name="UserConfigFile")
SystemConfigFile = ConfigLayer(name="SystemConfigFile")

layers = (CommandLineConfigFile, CommandLineArgs, EnvVar, UserConfigFile, SystemConfigFile,)

sys_conf = PidanConfig(conn="http://abc", src=SystemConfigFile)
user_conf = PidanConfig(conn="http://bcd", mode="全量", src=UserConfigFile)


pidan_conf_system = RecordArray2[PidanConfig].build(layers)
pidan_conf_system.place(sys_conf)
pidan_conf_system.place(user_conf)

class PidanConfigSystem(RecordArray2):
    CommandLineConfigFile: PidanConfig | None = None
    CommandLineArgs: PidanConfig | None = None
    EnvVar: PidanConfig | None = None
    UserConfigFile: PidanConfig | None = None
    SystemConfigFile: PidanConfig | None = None
