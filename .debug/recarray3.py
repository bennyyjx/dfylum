from __future__ import annotations

from typing import TYPE_CHECKING, TypeVar

if TYPE_CHECKING:
    pass

from typing import Generic

from pydantic import BaseModel

T = TypeVar("T")
class RecordList2(list, Generic[T]):
    def __init_subclass__(cls, size):
        if size != 5:
            raise AssertionError
        super().__init_subclass__()



class ConfigLayer(BaseModel):
    name: str


class Element(BaseModel):
    src: ConfigLayer

ElemT = TypeVar("ElemT", bound=Element)

class RecordArray3(MutableArray, Generic[ElemT]):
    _elem_cls: type
    _elem_src_field: str = "src"
    _layers: list = None
    _size: int = None

    class Config:
        underscore_attrs_are_private = True

    @classmethod
    def build(cls, layers) -> RecordArray3[ElemT]:
        self = cls()
        self._layers = layers
        self._size = len(layers)
        self._data = [None] * self._size
        return self

    def calc_idx(self, val: ElemT) -> int:
        src = getattr(val, self._elem_src_field)
        try:
            idx = self._layers.index(src)
        except ValueError as e:
            raise e
        else:
            return idx

    def place(self, val: ElemT) -> RecordArray3[ElemT]:
        idx = self.calc_idx(val)
        self._data[idx] = val
        return self

    def merge(self) -> ElemT:
        ...

