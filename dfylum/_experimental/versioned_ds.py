from typing import Protocol, TypeVar, Literal
from datetime import datetime
import pandas as pd



T = TypeVar("T")

class TimestampedObject(Protocol[T]):
    """
    timestamp is always present.
    tag is optional; implement a tag() method to tag a value at a specific timestamp.
    
    when trying to access a timestamped value that is earlier than the earliest available, return None for the moment, but this should be improved. 
    """
    # TODO: tag: str -> tag: VersionTag
    
    def get(self, as_of: datetime | None = None, tag: str | None = None) -> T | None: 
        """
        tag will take precedence over as_of if both are present, and will check vailidity first
        """    
        pass

    def set(self, val:T, timestamp: datetime | None, tag: str | None) -> None: 
        """
        Append-only, does not allow update/insertion at earlier date.
        """
        pass

    def tag(self, timestamp: datetime, tag: str) -> None: 
        pass


class TimestampedObjectImplPandas(TimestampedObject[T]):
    _latest: T
    _latest_timestamp: datetime
    _earliest_timestamp: datetime
    _history: pd.DataFrame

    def __init__(self, value: T, timestamp: datetime | None, tag: str | None) -> None:
        timestamp = timestamp if timestamp is not None else datetime.now()
        
        self._latest = value
        self._latest_timestamp = timestamp
        self._earliest_timestamp = timestamp

        self._history = self._create_history_record_df(value, timestamp, tag)
    
    def _create_history_record_df(self, value: T, timestamp: datetime | None = None, tag: str | None = None) -> pd.DataFrame:
        return pd.DataFrame([(timestamp, tag, value)], columns=["timestamp", "tag", "value"])

    def _detect_timestamp_tag_conflict(self, as_of: datetime, tag: str) -> bool:
        # TODO: implement
        return False
    
    def _detect_param_combo(self, as_of: datetime, tag: str) -> Literal["none", "as_of", "tag", "both"]:
        match (as_of, tag):
            case (None, None): return "none"
            case (datetime(), None): return "as_of"
            case (None, str()): return "tag"
            case (datetime(), str()): return "both"
            case _: raise Exception

    def _get_by_tag(self, tag: str) -> T:
        return self._history.loc[self._history["tag"]==tag, "value"][0]

    def _get_by_timestamp(self, as_of: datetime) -> T:
        if as_of >= self._latest_timestamp:
            return self._latest
        if as_of <self._earliest_timestamp:
            return None
        
        idx = self._history["timestamp"].sub(as_of).abs().idxmin() # i.e.: argmin
        return self._history.loc[idx, "value"][0]

    def get(self, as_of: datetime | None = None, tag: str | None = None) -> T | None:
        param_combo = self._detect_param_combo(as_of, tag)
        
        match param_combo:
            case "none":
                return self._latest
            
            case "both":
                assert not self._detect_timestamp_tag_conflict(as_of, tag)
                return self._get_by_tag(tag)
            
            case "tag": 
                return self._get_by_tag(tag)
            
            case "as_of": 
                return self._get_by_timestamp(as_of)    

            case _:
                raise Exception

    def set(self, value: T, timestamp: datetime | None = None, tag: str | None = None) -> None:
        timestamp = timestamp if timestamp is not None else datetime.now()
        
        # should not allow updating value at earlier timestamp
        if timestamp <= self._latest_timestamp:
            raise Exception

        self._latest = value
        self._latest_timestamp = timestamp
        self._history = pd.concat(self._history, self._create_history_record_df(value, timestamp, tag))

    def tag(self, timestamp: datetime, tag: str) -> None:
        # timestamp should exist in self._history
        # the selected record's tag field should either be None or match the incoming tag 
        if timestamp not in self._history["timestamp"]:
            raise Exception
        
        record = self._history.loc[self._history["timestamp"]][0]

        if record["tag"] == tag:
            return
        elif record["tag"] is None:
            self._history.loc[self._history["timestamp"] == timestamp, "tag"] = tag
        else:
            raise Exception

