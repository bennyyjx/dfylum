from collections import namedtuple
from typing import Sequence, TypeAlias, TypeVar, NamedTuple, Type
from typing_extensions import Self

T = TypeVar("T")
PseudoRowT: TypeAlias = NamedTuple
PseudoColumnT: TypeAlias = tuple


class PseudoDataFrame:
    _store: list[namedtuple]
    _ncols: int
    _cls: Type[namedtuple]
    _name: str

    def __init__(self, name: str, columns: Sequence[str], vals: Sequence | None = None) -> None:
        # initial implementation: only allow initializing one row

        assert len(vals) == len(columns)
        self._ncols = len(vals)

        Record = namedtuple(name, columns)
        self._name = name
        self._store = []
        self._cls = Record

        if vals:
            self.append(vals)

    def _clone_structure(self) -> Self:
        from copy import copy
        _clone = copy(self)
        _clone._store = []
        return _clone

    def _assert_idx_validity(self, idx: int) -> None:
        assert 0 <= idx <= len(self._store) - 1

    def _assert_idxs_validity(self, idxs: Sequence[int]) -> None:
        idxs_sorted = sorted(idxs)
        self._assert_idx_validity(idxs_sorted[0])
        self._assert_idx_validity(idxs_sorted[-1])

    def append(self, data: tuple | Sequence[tuple]) -> None:
        match data:
            case tuple():
                # TODO: 存储应使用plain tuple, 通过api获取行时才转为namedtuple
                rec = self._cls(*data)
                self._store.append(rec)
                return
            case list():
                for rec in data:
                    if len(rec) != self._ncols:
                        raise Exception
                for rec in data:
                    self.append(rec)
            case _:
                raise Exception

    def col(self, col_name: str) -> tuple:
        """Select single column."""
        assert col_name in self._cls._fields
        return tuple(getattr(rec, col_name) for rec in self._store)

    def row(self, idx: int) -> namedtuple:
        """Select single row."""
        self._assert_idx_validity(idx)
        return self._store[idx]

    def select(self, selector: str | Sequence[str]) -> tuple | Self:
        """Select column(s) by name."""
        match selector:
            case str():
                return self.col(selector)
            case list() | tuple():
                # result = self._clone_structure()
                # result.append()
                raise NotImplementedError

    def filter(self, selector: int | Sequence[int] | range) -> Self:
        """Select rows by idx(s) & range"""
        if isinstance(selector, range):
            selector = tuple(selector)
        match selector:
            case int():
                self._assert_idx_validity(selector)
                return self.row(selector)
            case list() | tuple():
                self._assert_idxs_validity(selector)
                result = self._clone_structure()
                for idx in selector:
                    result.append(self.row(idx))
                return result
            case _:
                raise Exception
