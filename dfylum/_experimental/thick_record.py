import enum
from typing import Generic, TypeVar, TypeAlias, ClassVar, Type

from pydantic import BaseModel, PositiveInt
from pydantic.generics import GenericModel
from dfylum.common import Uninitialized, UNINITIALIZED, OrderedEnum, NOT_APPLICABLE

ElemT = TypeVar("ElemT", bound=BaseModel)
CollationT = TypeVar("CollationT",
                     bound=enum.EnumMeta)
OptionalElemT: TypeAlias = ElemT | Uninitialized


class ThickRecord(GenericModel, Generic[ElemT, CollationT]):
    """
    ThickRecord is better implemented using RecordArray
    """
    __root__: list[ElemT | Uninitialized]
    _elem_cls: ClassVar[Type[ElemT]]
    _collation_src: ClassVar[Type[OrderedEnum]]
    _collation: ClassVar[tuple[CollationT]]
    _element_sort_field: ClassVar[str] = 'src'  # TODO: remove hard code
    _size: ClassVar[PositiveInt]

