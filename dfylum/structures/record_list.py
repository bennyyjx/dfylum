from typing import Generic, TypeVar, Type, ClassVar

from pydantic import BaseModel

T = TypeVar("T", bound=BaseModel)


class RecordList(list, Generic[T]):
    _cls: ClassVar[type[BaseModel]]
    _id_field: ClassVar[str]
    def __init_subclass__(cls, *, elem_cls: type[BaseModel], id_field: str, **kwargs):
        assert id_field in elem_cls.__fields__
        super().__init_subclass__(**kwargs)
        cls._cls = elem_cls
        cls._id_field = id_field

    def __init__(self, *args):
        super().__init__(args)
        for elem in args:
            if (not isinstance(elem, self._cls)) and (elem is not None):
                raise ValueError("ReceordList require elements of the same type. Please check.")
            else:
                print("hi")
    def _rows_are_uniquely_identified(self):
        id_col = self.col(self._id_field)
        if len(set(id_col)) < len(self):
            return False
        return True

    def row_index(self):
        assert self._rows_are_uniquely_identified()
        return self.col(self._id_field)

    @property
    def col_index(self) -> tuple[str, ...]:
        return tuple(self._cls.__fields__)

    def col(self, col_name: str) -> tuple:
        """Select single column."""
        assert col_name in self.col_index
        return tuple(getattr(rec, col_name) for rec in self)

    def row(self, idx: int) -> T:
        """Select single row."""
        return self[idx]

