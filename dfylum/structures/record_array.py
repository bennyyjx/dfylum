from __future__ import annotations

from typing import TYPE_CHECKING

import attrs
from typing_extensions import Self

if TYPE_CHECKING:
    pass

from typing import Generic, Sequence

from dfylum.common import ElemT, MutableArray, IndexedArray, VT


class RecordArrayBase(MutableArray, Generic[ElemT]):
    _row_idx: Sequence
    _elem_cls: type[ElemT]

    def __init__(self,
                 *args: ElemT | None,
                 row_idx: Sequence,):
        size = len(row_idx)
        if not args:
            args = [None] * size
        else:
            for arg in args:
                if arg is not None:
                    self._elem_cls = type(arg)

        super().__init__(*args, size=size)
        self._row_idx = row_idx

    def calc_idx(self, val: ElemT) -> int:
        src = getattr(val, "_src")
        try:
            idx = self._row_idx.index(src)
        except ValueError as e:
            raise e
        else:
            return idx

    def place(self, val: ElemT) -> Self:
        idx = self.calc_idx(val)
        self[idx] = val
        return self

    def select(self):
        ...

    @property
    def row_names(self) -> Sequence[str]:
        return self._row_idx

    @property
    def col_names(self) -> Sequence[str]:
        if not self._elem_cls:
            raise ValueError
        return tuple(self._elem_cls.__fields__)


@attrs.define
class RecordArray(RecordArrayBase, Generic[ElemT]):
    def __init__(self, *args):
        row_idx = [_attr.name for _attr in attrs.fields(type(self))]
        assert all(not isinstance(name, int) for name in row_idx), "Row index cannot be an integer."
        assert {elem._src for elem in args} <= set(row_idx)

        idx_elem_map = {
            elem._src: elem for elem in args
        }

        self.__attrs_init__(**idx_elem_map)

        super().__init__(*tuple(attrs.asdict(self).values()), row_idx=row_idx)

    def __getitem__(self, key):
        if isinstance(key, int):
            return super().__getitem__(key)
        return getattr(self, key)

    def __setitem__(self, key, val: ElemT | None):
        if isinstance(key, int):
            row_name = self.row_names[key]
            setattr(self, row_name, val)
            super().__setitem__(key, val)
        elif key in self.row_names:
            setattr(self, key, val)
            idx = self._row_idx.index(key)
            return super().__setitem__(idx, val)
        else:
            raise KeyError

        return self

    def __delitem__(self, key):
        """
        Behavior differs from builtin list: instead of shortening the list when deleting a key, it will preserve the key and set the correspoding value to None, due to the fact that a RecordArray's size is pre-determined at class creation time.
        """
        self[key] = None
        return self

    def store(self, elem: ElemT):
        self[elem._src] = elem
        return self

    def get_column(self, name: str) -> IndexedArray[str, VT]:
        assert name in self._elem_cls.__fields__
        res = {self._row_idx[i]: getattr(elem, name, None) for i, elem in enumerate(self)}
        return IndexedArray(**res)

    def merge_one_col(self, name: str):
        for val in self.get_column(name):
            if val is not None:
                return val
        return None

    def merge(self) -> ElemT:
        merged = {col: self.merge_one_col(col) for col in self.col_names}
        elem = self._elem_cls(**merged)
        elem._src = "Merged"
        return elem

    def merge_with_detail(self):
        """
        Merge while keeping which "layer" each merged field value comes from.
        """
        raise NotImplementedError

    def view(self):
        """Transpose the 'dataframe': put PidanConfig's fields to row index and layer keys to column index."""


