import enum
from pydantic import Field
from typing import Generic, TypeVar, Type, ClassVar, TypeAlias, NoReturn, Collection, Union

from pydantic import BaseModel, PositiveInt, create_model, validator
from pydantic.generics import GenericModel
from dfylum.common import Uninitialized, UNINITIALIZED, OrderedEnum, NOT_APPLICABLE

Collation: TypeAlias = OrderedEnum
CollationT = TypeVar("CollationT", bound=Collation)


class Element(BaseModel):
    src: Collation

    class Config:
        arbitrary_types_allowed = True


ElemT = TypeVar("ElemT", bound=Element)
ElemClsT = TypeVar("ElemClsT", bound=type[Element])
OptionalElemT: TypeAlias = ElemT | Uninitialized


class MergeState(enum.Enum):
    NEVER_MERGED = enum.auto()
    MERGED = enum.auto()


class InitState(enum.Enum):
    UNINITIALIZED = enum.auto()
    INITIALIZING = enum.auto()
    INITIALIZED = enum.auto()


class RecordArrayBase(GenericModel, Generic[ElemT]):
    """
    Similar to array[Record], but Record must be a subclass of the `Element` class.

    Features:
    1. can be incrementally initialized
    """
    __root__: list[ElemT | Uninitialized] = Field(default_factory=list)
    _elem_cls: type[ElemT]
    _collation_src: type[OrderedEnum]
    _collation: tuple[Collation, ...]
    _element_collation_src_field: str = 'src'
    _size: PositiveInt
    _merge_state: MergeState = MergeState.NEVER_MERGED
    _init_state: InitState = InitState.UNINITIALIZED
    _merge_result: ElemT

    class Config:
        arbitrary_types_allowed = True
        underscore_attrs_are_private = True
        validate_all = True

    @validator('__root__')
    def sort_input(cls, v: Collection[ElemT] | None):
        if v == []:
            return [UNINITIALIZED] # TODO
        return sorted(v, key=lambda x: x.src, reverse=True)

    def __contains__(self, item):
        return item in self.__root__

    def __iter__(self):
        return iter(self.__root__)

    def __getitem__(self, idx: int) -> Union[ElemT, Uninitialized]:
        return self.__root__[idx]

    def __len__(self) -> int:
        return self._size

    def __get__(self, obj, objtype=None):
        return self.merged_value

    @property
    def merged_value(self) -> ElemT:
        if self.merge_state is not MergeState.MERGED:
            return self.merge()
        return self._merge_result

    @property
    def init_state(self) -> InitState:
        if UNINITIALIZED not in self:
            return InitState.INITIALIZED

        for item in self:
            if item is not UNINITIALIZED:
                return InitState.INITIALIZING

        return InitState.UNINITIALIZED

    @property
    def merge_state(self) -> MergeState:
        return self._merge_state

    # @cached_property
    @property
    def row_names(self) -> tuple[str, ...]:
        return tuple(x.name for x in self._collation)

    # @cached_property
    @property
    def col_names(self) -> tuple[str, ...]:
        return tuple(self._elem_cls.__fields__.keys())

    def col(self, col_name: str) -> tuple:
        """Select single column."""
        assert col_name in self.col_names

        result = tuple(
            getattr(elem, col_name) if elem is not UNINITIALIZED else NOT_APPLICABLE
            for elem in self
        )

        return result

    def push(self, elem: ElemT) -> None:
        elem_src: Collation = getattr(elem, self._element_collation_src_field)
        idx = self._collation.index(elem_src)
        self.__root__[idx] = elem

    def merge_one_col(self, col_name: str):
        for val in self.col(col_name):
            if val is not NOT_APPLICABLE:
                return val

    def merge(self) -> ElemT:
        merged_vals = tuple(self.merge_one_col(col_name) for col_name in self.col_names)
        named_merged_vals = dict(zip(self.row_names, merged_vals))
        merged_elem: ElemT = self._elem_cls(**named_merged_vals)

        self._merge_result = merged_elem
        self._merge_state = MergeState.MERGED

        return merged_elem


def build_concrete_record_array_cls(name: str,
                                    elem_cls: type[Element],
                                    collation_src: Type[OrderedEnum]) -> Type[RecordArrayBase]:
    _size = len(collation_src)

    Model: type[RecordArrayBase] = create_model(
        name,
        __base__=(RecordArrayBase[ElemT], Generic[ElemT]),
        __root__=[UNINITIALIZED] * _size,
        _elem_cls=elem_cls,
        _collation_src=collation_src,
        _collation=tuple(collation_src.__members__.values()),
        _size=_size
    )
    # TODO: the precise type annotation of _collation has to be modified after class creation

    assert Model._element_collation_src_field in Model._elem_cls.__fields__

    return Model
