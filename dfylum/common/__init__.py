from __future__ import annotations

import enum
from abc import ABC
from functools import total_ordering, cached_property
from typing import Literal, Type, TypeAlias, Generic, Any, TypeVar

from pydantic import BaseModel, PrivateAttr
from typing_extensions import Self
from dfylum.utils.sentinel import SentinelValue
from dfylum.utils.src import get_sliced_items


class Undefined(SentinelValue):
    """A sentinel class with the meaning of "undefined"."""


class Uninitialized(SentinelValue):
    """A sentinel class with the meaning of "uninitialized"."""


class NotApplicable(SentinelValue):
    """A sentinel class with the meaning of "uninitialized"."""


UNDEFINED = Undefined(__name__, "UNDEFINED")
UNINITIALIZED = Uninitialized(__name__, "UNINITIALIZED")
NOT_APPLICABLE = NotApplicable(__name__, "NOT_APPLICABLE")


@total_ordering
class OrderedEnum(enum.Enum):
    """
    Base Enum Class that implement ordering by member definition order;
    Field defined first is the largest.
    """

    @cached_property
    def collation(self) -> tuple[Type[Self], ...]:
        return tuple(reversed(self.__class__.__members__.values()))

    def __lt__(self, other):
        if isinstance(other, OrderedEnum):
            self_def_idx = self.collation.index(self)
            other_def_idx = self.collation.index(other)

            return self_def_idx < other_def_idx

        raise ValueError


class QBaseModel(BaseModel):
    def __init__(self, **data):

        public_fields = self.__fields__
        private_fields = self.__private_attributes__
        data_public = dict(get_sliced_items(data, public_fields))
        data_private = dict(get_sliced_items(data, private_fields))

        super().__init__(**data_public)
        if len(data_private) > 0:
            for k, v in data_private.items():
                setattr(self, k, v)


class Element(QBaseModel):
    _src: str = PrivateAttr(default=None)


ElemT = TypeVar("ElemT", bound=QBaseModel)


class MutableArray(list, Generic[ElemT], ABC):
    _size: int

    def __init__(self, *args, size: int):
        super().__init__(args)
        self._size = size

    def append(self, obj: Any):
        raise ValueError

    def insert(self, obj: Any, idx: int):
        raise ValueError


KT = TypeVar("KT")
VT = TypeVar("VT")


class IndexedArray(dict[KT, VT]):
    def __init__(self, **kwargs):
        super().__init__(kwargs)

    def __iter__(self):
        """
        Iterate over 'values' instead of 'keys'.
        """
        return iter(self.values())
