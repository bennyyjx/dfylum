from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Sequence, Iterator


def get_sliced_items(d:dict, keys: Sequence, ignore_missing_key=True) -> Iterator:
    """按照指定的keys的顺序，返回字典键值对"""
    # if len(keys) == 0:
    #     return (d, )
    if ignore_missing_key:
        for k in keys:
            if (v := d.get(k)) is not None:
                yield k, v
    else:
        return ((k, d[k]) for k in keys)
