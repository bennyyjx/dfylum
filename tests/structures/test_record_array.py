import attrs
import pytest

from dfylum.structures.record_array import RecordArray
from dfylum.common import Element


class Config(Element):
    conn: str | None = None
    mode: int | None = None
    threshold: float | None = None


@attrs.define(init=False)
class ConfigStore(RecordArray[Config]):
    CommandLineConfigFile: Config = None
    CommandLineArgs: Config = None
    EnvVar: Config = None
    UserConfigFile: Config = None
    SystemConfigFile: Config = None
    ApplicationDefault: Config = None


conf_cli_cf = Config(conn="cli_cf", mode=1, threshold=1.5, _src="CommandLineConfigFile")
conf_cli_args = Config(conn="cli_args", _src="CommandLineArgs")
conf_env_var = Config(conn="env_var", threshold=0.5, _src="EnvVar")
conf_user = Config(conn="user", mode=0, threshold=-2.0, _src="UserConfigFile")
conf_sys = Config(conn="sys", mode=3, threshold=0.2, _src="SystemConfigFile")

store_complete = ConfigStore(conf_sys, conf_cli_args, conf_env_var, conf_user, conf_cli_cf)


@pytest.fixture
def rec_array_subclass():
    return ConfigStore


def test_initialization(rec_array_subclass: type[RecordArray]):
    ConfigStore()
    store = ConfigStore(conf_user, conf_cli_args)

    assert list(store) == [None, conf_cli_args, None, conf_user, None, None]


def test___getitem___method():
    store = ConfigStore(conf_sys, conf_env_var, conf_cli_args)
    assert store[0] is None
    assert store[1] == conf_cli_args
    assert store[2] == conf_env_var
    assert store[3] is None
    assert store[4] == conf_sys

    assert store["CommandLineConfigFile"] is None
    assert store["CommandLineArgs"] == conf_cli_args
    assert store["EnvVar"] == conf_env_var
    assert store["UserConfigFile"] is None
    assert store["SystemConfigFile"] == conf_sys


def test___setitem___method():
    store1 = ConfigStore(conf_sys)
    store2 = ConfigStore(conf_sys, conf_env_var, conf_cli_args)
    store1[2] = conf_env_var
    store1[1] = conf_cli_args
    assert store1 == store2

    store1 = ConfigStore(conf_sys)
    store2 = ConfigStore(conf_sys, conf_env_var, conf_cli_args)
    store1["EnvVar"] = conf_env_var
    store1["CommandLineArgs"] = conf_cli_args
    assert store1 == store2

def test___delitem___method():
    store1 = ConfigStore(conf_sys, conf_env_var, conf_cli_args)
    store2 = ConfigStore(conf_sys, conf_cli_args)
    store3 = ConfigStore(conf_cli_args)

    del store1[2]
    assert store1 == store2

    del store1["SystemConfigFile"]
    assert store1 == store3

def test_store_method(rec_array_subclass):
    store1 = ConfigStore(conf_cli_args, conf_user)

    store2 = ConfigStore(conf_user)
    store2 = store2.store(conf_cli_args)

    assert store1 == store2


def test_merge():
    store1 = ConfigStore(conf_cli_args, conf_user)
    assert store1.merge() == Config(conn="cli_args", mode=0, threshold=-2.0, _src="Merged")

    assert store_complete.merge() == Config(conn="cli_cf", mode=1, threshold=1.5, _src="Merged")
