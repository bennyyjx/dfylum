import enum

import pytest
from pydantic import BaseModel

from dfylum._legacy.record_array import build_concrete_record_array_cls, Element
from dfylum.common import UNINITIALIZED, OrderedEnum, Uninitialized


class ElemSource(OrderedEnum):
    SRC1 = enum.auto()
    SRC2 = enum.auto()
    SRC3 = enum.auto()
    SRC4 = enum.auto()


class Elem(Element):
    f1: str
    f2: int
    src: ElemSource


def test_build_concrete_record_array_cls():


    RecordArrayCls = build_concrete_record_array_cls(name="RecordArrayCls", elem_cls=Elem, collation_src=ElemSource)
    arr = RecordArrayCls()

    assert len(arr) == 4
    assert arr.__root__ == [UNINITIALIZED, UNINITIALIZED, UNINITIALIZED, UNINITIALIZED]

    elem1 = Elem(f1='f1', f2=1, src=ElemSource.SRC1)
    elem2 = Elem(f1='f2', f2=2, src=ElemSource.SRC2)
    elem3 = Elem(f1='f3', f2=3, src=ElemSource.SRC3)
    elem4 = Elem(f1='f4', f2=4, src=ElemSource.SRC4)

    arr.push(elem2)
    arr.push(elem4)
    arr.push(elem1)
    arr.push(elem3)

    assert arr.__root__ == [elem1, elem2, elem3, elem4]
    assert tuple(arr) == (elem1, elem2, elem3, elem4)


def test_build_concrete_record_array_assert_elem_sort_field():
    class ElemSource(OrderedEnum):
        SRC1 = enum.auto()
        SRC2 = enum.auto()
        SRC3 = enum.auto()
        SRC4 = enum.auto()

    class Elem(Element):
        f1: str
        f2: int
        src: ElemSource

    class ElemInvalid(Element):
        f1: str
        f2: int
        src1: ElemSource

    assert build_concrete_record_array_cls(name="RecordArrayCls", elem_cls=Elem, collation_src=ElemSource)

    with pytest.raises(AssertionError):
        build_concrete_record_array_cls(name="RecordArrayCls", elem_cls=ElemInvalid, collation_src=ElemSource)


def test_build_concrete_record_array_cls_get_one_col():
    class ElemSource(OrderedEnum):
        SRC1 = enum.auto()
        SRC2 = enum.auto()
        SRC3 = enum.auto()
        SRC4 = enum.auto()

    class Elem(BaseModel):
        f1: str | Uninitialized = UNINITIALIZED
        f2: int | Uninitialized = UNINITIALIZED
        f3: float | Uninitialized = UNINITIALIZED
        src: ElemSource

    elem1 = Elem(f1='f1', src=ElemSource.SRC1)
    elem2 = Elem(f2=2, src=ElemSource.SRC2)
    elem3 = Elem(f3='f3', f2=3, src=ElemSource.SRC3)
    elem4 = Elem(f1='f4', f2=4, f3=5.5,src=ElemSource.SRC4)

def test_build_concrete_record_array_cls_merge_one_col():
    pass


def test_build_concrete_record_array_cls_merge():
    pass
