# `dfylum`: Dataframe-like data structure without `pandas`

## Etymology
> `dfylum` = `df` (DataFrame) + `phylum` (a broad level of category),
> meaning "a phylum of dataframe-like data structures."

## Installation

```shell
git clone ...

cd ...

pip install -e .
```

## Status

| structure      |              maturity |
|:---------------|----------------------:|
| `RecordArray`  |  implemented & tested |
| `RecordList`   |           implemented |

## Usage

### `RecordArray`

`RecordArray` is a sized 1-dimensional array with the following features:

1. holds elements of the same type; currently the element type is constrained to instance of `Element` or its subclass.
2. `None` is allowed as an element.
3. each element is analogous to a "row" in a dataframe.
4. row index must be specified statically: specifying your row index as fields to a subclass of `RecordArray`.
5. from a dataframe point of view, the column index corresponds to the element's fields, while the row index corresponds to the subclassed `RecordArray`'s fields.
6. user of `RecordArray` needs to specify a `_src` field (defined in `Element`), which corresponds to one of the row index (i.e. one of the fields defined in the subclass of `RecordArray`)

The primary use case for `RecordArray` is to drastically ease the merging of application configuration. By pre-defining configuration sources in the application, user can easily collect configurations at different places of the program, simply store it in the `RecordArray`, and the merged configuration is one `.merge()` call away.

See the example below for such usage.

#### Example

```python
import attrs
from dfylum.structures.record_array import RecordArray
from dfylum.common import Element

# Define your configuration
class Config(Element):
    conn: str | None = None
    mode: int | None = None
    threshold: float | None = None


# Define your ConfigStore class
@attrs.define(init=False)
class ConfigStore(RecordArray[Config]):
    CommandLineConfigFile: Config = None
    CommandLineArgs: Config = None
    EnvVar: Config = None
    UserConfigFile: Config = None
    SystemConfigFile: Config = None
    ApplicationDefault: Config = None

# Assume you have collected & parsed the configuration from various sources
conf_cli_cf = Config(conn="cli_cf", mode=1, _src="CommandLineConfigFile")
conf_cli_args = Config(conn="cli_args", _src="CommandLineArgs")
conf_env_var = Config(conn="env_var", threshold=0.5, _src="EnvVar")
conf_user = Config(conn="user", mode=0, threshold=-2.0, _src="UserConfigFile")
conf_sys = Config(conn="sys", mode=3, threshold=0.2, _src="SystemConfigFile")
conf_app_default = Config(conn="app_default", mode=1, threshold=1.5, _src="ApplicationDefault")

# Start the application with a ConfigStore that holds application default config
conf_store = ConfigStore(conf_app_default)

# Push system configuration into the store
conf_store.store(conf_sys)

# Push user configuration & environment variable conf into the store; the store method mutates the object, but also returns itself.
conf_store = conf_store.store(conf_user).store(conf_env_var)

# Push cli argument config into the store; `RecordArray` supports square brackets access, just like a list
conf_store['CommandLineArgs'] = conf_cli_args

# Let's produce a merged configuration
conf_merged = conf_store.merge()

# Check the results
assert conf_merged == Config(conn="cli_args", mode=0, threshold=0.5, _src="Merged")

```

